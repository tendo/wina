/*
	common.h
	Toshinori Endo
	96.7.7
*/

#if !defined(__COMMON_H__)
#define __COMMON_H__

int SharedSiteOnly = 1;

#include <iostream>
#include <fstream>
#include <string.h>
#include <cctype>
#include <stdio.h>
#include <stdlib.h>

#define nl <<"\n"
#define sp <<" "

int LINELEN = 60;
int NAMELEN = 14;
const int SEQLEN = 10000;
const int MAXSEQ = 1000;
const int BUFLEN = 1000;

enum {NONE=0,SEQUENCE,ALIGNMENT,QUIT};
enum {NO,YES,BAD,OK};

inline const int base(int X){
    const char *base="acgt";
    return (0<=X && X<4)?base[X]:'?';
}

/*
	Nucleotide base expressions

	Code   Meanig
	 A	A
	 G	G
	 C	C
	 T	T
	 U	U
	 R	A or G (puRine)
	 Y	C or T (pYrimidine)
	 M	A or C (aMino group)
	 K	G or T (Keto group)
	 S	C or G (Strong hydrogen bond)
	 W	A or T (Weak hydrogen bond)
	 H	A,C or T (not G)
	 B	A,C,or T (not A)
	 V	A,C or G (not T nor U)
	 D	A,G or T (not C)
	 N	A,C,G or T (aNy)
*/

inline int isbase4(char x){ return strchr("ACGTU",toupper(x))?YES:NO;}
inline int isbase(char x)
	{ return strchr("AGCTURYMKSWHBVDN",toupper(x))?YES:NO;}
inline int isaa(char x){ return strchr("ACDEFGHIKLMNPQRSTVWXYZ",x)?YES:NO;}
inline int isgap(char x){ return strchr(".-*",x) ? YES : NO; }
inline int conv(char x)
	{ return isbase(x)?(x=='C')*1+(x=='G')*2+(x=='T')*3:UNKNOWN;}

inline int BaseNum(char X){
    switch (tolower(X)){
      case 'a': return 0;
      case 'c': return 1;
      case 'g': return 2;
      case 't':
      case 'u': return 3;
      default:  return -1;
    }
}

inline int DifferentSites(const char* s1,const char* s2,int n=3){
    int d=0;
    while (n-->0 && *s1 && *s2)
	if (*s1++ != *s2++) d++;
    return d;
}

inline int DifferentSites(int p1,int p2,int p3,int p4,int p5,int p6){
    int d=0;
    if (p1 != p4) d++;
    if (p2 != p5) d++;
    if (p3 != p6) d++;
    return d;
}

class Sequence {
    int len;
    char *name;
    char *seq;
  public:
    Sequence(const char* newname,const char* newseq="");
    ~Sequence(){ delete[] name; delete[] seq;}
    const int Length() const {return len;}
    const char* Name() const {return name;}
    const int AskName(const char* name1) const
	{return strncmp(name,name1,NAMELEN)?NO:YES;}
    const char* Seq(int s=0) const {return seq+s;}
    const char* Seq(char* str,int s=0,int e=SEQLEN-1) const;
    virtual void AddSeq(const char* seq1);
    virtual void Print(int start=0,int end=0) const;
};

class SeqArray {
    int number;
    int Min;
    int Max;
    Sequence* element[MAXSEQ];
    int Shared[SEQLEN];
    void SharedSiteCheck(int n,int start=0);
    void SharedSiteCheck();

  public:
    SeqArray();
    ~SeqArray() { for (int i=0; i<Num(); i++) delete element[i];}
    virtual int ReadAln(std::istream&);
    int Num() const { return number;}
    int IsShared(int i) const
	{ return (i>=0 && i<SEQLEN)? Shared[i]: NO;}
    virtual void SetShared(int i) { if (i>=0 && i<SEQLEN) Shared[i]=YES;}
    virtual void ResetShared(int i) { if (i>=0 && i<SEQLEN) Shared[i]=NO;}
    Sequence* Elem(int n) const
	{ if (0<=n && n<=number) return element[n]; }
    Sequence operator[](int n) const { return *element[n];}
    const char* Name(int n) const { return Elem(n)->Name();}
    const char* Seq(int n,int s=0) const { return Elem(n)->Seq(s);}
    int MinLength() const { return Min;}
    int MaxLength() const { return Max;}
    int CheckSequenceLength();
    int CheckSequence(int,int);
    Sequence* AddSeq(const char*,const char*);
    void Print(int x) const { Elem(x)->Print();}
    void Print() const { for (int i=0; i<Num(); i++) Print(i);}
};

class SeqArrayC: public SeqArray {
  public:
    SeqArrayC(){}
    ~SeqArrayC(){}
    void SetShared(int i) {
	i=(i/3)*3;	 // making sure i is a multiple of 3.
	SeqArray::SetShared(i);
	SeqArray::SetShared(i+1);
	SeqArray::SetShared(i+2);
    }
    void ResetShared(int i) {
	i=(i/3)*3;	 // making sure i is a multiple of 3.
	SeqArray::ResetShared(i);
	SeqArray::ResetShared(i+1);
	SeqArray::ResetShared(i+2);
    }
    int CheckSequenceLength();
};

Sequence::Sequence(const char* newname,const char* newseq){
    name=new char[NAMELEN+1];
    strncpy(name,newname,NAMELEN);
    name[NAMELEN] = 0;

    seq=new char[SEQLEN+1];
    len=0; *seq=0;
    AddSeq(newseq);
}

void Sequence::AddSeq(const char* seq1){
    if (Length()+strlen(seq1)>=SEQLEN){
	std::cerr << "Sequence too long: " << name nl;
	std::cerr << Length() << ',' << strlen(seq1) nl;
	exit(1);
    }
    char* p=seq;
    int bases=0;
    while (*p) p++; // obtain end of sequence.
    while (*seq1 && p-seq < SEQLEN){
	if (isbase(*seq1)) {
	    *p++ = toupper(*seq1);
	    bases++;
	    if (!isbase4(*seq1))
		fprintf(stderr,"%s: Non-regular base: %c at %d.\n",
			Name(),*seq1,bases);
	} else if (isgap(*seq1))
	    *p++ = '-';
	seq1++;
    }
    *p = 0;
    len = p-seq;
    if (p-seq>=SEQLEN) {
	std::cerr << "Sequence " << Name() << " cut off at "
	     << Length() << "\n."
	     << "Change SEQLEN in common.cc and recompile"
	     << " for longer sequences.\n";
    }
}

const char* Sequence::Seq(char* str,int s,int e) const {
    if (e<0||e>Length()) { e=Length(); }
    strncpy(str,seq+s,e-s+1);
    str[e-s+1] = 0;
    return str;
}

void Sequence::Print(int start,int end) const {
    if (Length()>0){
	if (start<0 && start+Length()>0)
	    start+=Length();
	else if (start<0)
	    start=0;
	else if (start>Length())
	    start=Length()-1;
 
	if (end<0 && end+Length()>start)
	    end+=Length();
	else if (end==0 || end>Length())
	    end=Length();
	else if (end<=start)
	    end=start+1;
 
	std::cout << '>' << Name() << "\t" << end-start
	     << " (" << start+1 << "-->" << end << ')' nl;

	for (int i=start; i<end; i+=LINELEN) {
	    for (int j=0; i+j<end && j<LINELEN; j++)
		std::cout << *Seq(i+j);
	    std::cout nl;
	}     
    }
    else
	std::cout << '>' << Name() << "\t" << end-start nl;
    std::cout << "//" nl;
}
 
SeqArray::SeqArray(){
    number = 0;
    for (int i=0; i<SEQLEN; i++)
	SetShared(i);
}

void SeqArray::SharedSiteCheck(int n,int start){
    const char* p=Elem(n)->Seq(start);
    for (int i=start; i<Elem(n)->Length(); i++,p++)
	if (!isbase(*p))
	    ResetShared(i);
}

void SeqArray::SharedSiteCheck(){
    for (int i=0; i<MinLength(); i++)
	SetShared(i);
    for (int j=0; j<Num(); j++)
	SharedSiteCheck(j);
}

Sequence* SeqArray::AddSeq(const char* newname,const char* newseq){
    for (int i=0; i<number; ++i){
	if (element[i]->AskName(newname)==YES){
	    int len=element[i]->Length();
	    element[i]->AddSeq(newseq);
	    if (CheckSequenceLength()==OK)
		SharedSiteCheck(i,len);
	    return element[i];
	}
    }
    // new seqeunce registration.
    element[number] = new Sequence(newname,newseq);
    if (CheckSequenceLength()==OK)
	SharedSiteCheck(number);
    return element[number++];
}

int SeqArray::ReadAln(std::istream& f) {
    int Read = NONE;
    char buf[BUFLEN+1];
    const char* LETTERS="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._-";

    while (f.getline(buf,BUFLEN,'\n')){
	if (isspace(*buf) || *buf=='>')
	    ;
	else if (Read != ALIGNMENT && strstr(buf, "alignment"))
	    Read = ALIGNMENT;
	else if (strncmp(buf,"//",2)==0)
	    return Read;
	else if (strlen(buf)>NAMELEN)
	    AddSeq(buf,buf+NAMELEN);
    }
    return Read;
}

int SeqArray::CheckSequenceLength(){
    Max=Min=Elem(0)->Length();
    for (int i=0; i<Num(); i++){
	if (Min>Elem(i)->Length()) Min=Elem(i)->Length();
	if (Max<Elem(i)->Length()) Max=Elem(i)->Length();
    }
    if (Min == Max)
	return OK;
    else
	return BAD;
}
 
int SeqArrayC::CheckSequenceLength(){
    SeqArray::CheckSequenceLength();

    for (int i=0; i<Num(); i++)
        if (Elem(i)->Length() % 3 != 0)
            std::cerr << "Sequence lengths are not multiples of 3.\n  "
                 << Elem(i)->Name() << " (" << Elem(i)->Length() << ") \n";
    return OK;
}

int SeqArray::CheckSequence(int s1,int s2){
    if (Elem(s1)->Length() != Elem(s2)->Length()){
        std::cerr << "Sequence lengths mismatch between "
             << Elem(s1)->Name() << '(' << Elem(s1)->Length() << ") and "
             << Elem(s2)->Name() << '(' << Elem(s2)->Length() << ")\n";
    }
    return OK;
}
#endif
