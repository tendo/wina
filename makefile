CXX=g++ -g 
wina: wina.cc common.h SynDiffClass.h CodonTab.h synsite.h
	$(CXX) $(CPPFLAGS) $(CFLAGS) $< $(LDFLAGS) -o $@
test:
	wina test.aln
lp:
	mp -A4 wina.cc common.h SynDiffClass.h CodonTab.h synsite.h makefile\
	| lp
