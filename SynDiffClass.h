/*
	SynDiffClass.h
*/

#if !defined(_SYNDIFFCLASS_H_)
#define _SYNDIFFCLASS_H_

#include "CodonTab.h"
#include <stdio.h>

class SynDiffClass {
    double synsite[4][4][4];
    double syndiff[4][4][4][4][4][4];
    void DiffTable();
    void Diff0();
    void Diff1();
    void Diff2();
    void Diff3();
    void Stop();
    void SynSiteTable();
  public:
    SynDiffClass(){
	Diff0();
	Diff1();
	Diff2();
	Diff3();
	Stop();
	SynSiteTable();
    }
    double Diff(int a,int b,int c,int d,int e,int f) const
	{ return syndiff[a][b][c][d][e][f]; }
    double Diff(const char* s1, const char* s2) const {
	return Diff(BaseNum(s1[0]),BaseNum(s1[1]),BaseNum(s1[2]),
		    BaseNum(s2[0]),BaseNum(s2[1]),BaseNum(s2[2]));
    }
    void Print(int,int,int,int,int,int) const;
    void PrintAll() const;
};

// Initialize
void SynDiffClass::Diff0(){
    for (int p1=0;p1<4;p1++)
    for (int p2=0;p2<4;p2++)
    for (int p3=0;p3<4;p3++)
    for (int p4=0;p4<4;p4++)
    for (int p5=0;p5<4;p5++)
    for (int p6=0;p6<4;p6++)
	syndiff[p1][p2][p3][p4][p5][p6]=0;
}

// 1 different site.
void SynDiffClass::Diff1(){
    for (int p1=0;p1<4;p1++)
    for (int p2=0;p2<4;p2++)
    for (int p3=0;p3<4;p3++){

        // 1st position.
        for (int X=p1+1;X<4;X++)
            if (aa(X,p2,p3)==aa(p1,p2,p3))
                syndiff[p1][p2][p3][X][p2][p3]
                =syndiff[X][p2][p3][p1][p2][p3]
                =1;
 
        // 2nd position.
        for (int X=p2+1;X<4;X++)
            if (aa(p1,X,p3)==aa(p1,p2,p3))
                syndiff[p1][p2][p3][p1][X][p3]
                =syndiff[p1][X][p3][p1][p2][p3]
                =1;

        // 3rd position.
        for (int X=p3+1;X<4;X++)
            if (aa(p1,p2,X)==aa(p1,p2,p3))
                syndiff[p1][p2][p3][p1][p2][X]
                =syndiff[p1][p2][X][p1][p2][p3]
                =1;
    }
}

// 2 different site.
void SynDiffClass::Diff2(){
    int s; // synonymous subtotal
    int p; // pathways

    for (int p1=0;p1<4;p1++)
    for (int p2=0;p2<4;p2++)
    for (int p3=0;p3<4;p3++) if (aa(p1,p2,p3)==TER) continue; else
    for (int X=0;X<4;X++) if (X==p1) continue; else
    for (int Y=0;Y<4;Y++) if (Y==p2) continue; else
    for (int Z=0;Z<4;Z++) if (Z==p3) continue; else
    {
        // changes at 1st and 2nd positions.
	if (aa(X, Y, p3)!=TER) {
	    s=0;
	    p=0;
	    // 11X<->21X<->22X
	    if (aa(X,p2,p3)!=TER){
                p++;
                if (aa(p1,p2,p3)==aa(X ,p2,p3)) s++;
                if (aa(X ,p2,p3)==aa(X ,Y ,p3)) s++;
            }
	    // 11X<->12X<->22X
            if (aa(p1,Y,p3)!=TER){
                p++;
                if (aa(p1,p2,p3)==aa(p1,Y ,p3)) s++;
                if (aa(p1,Y ,p3)==aa(X ,Y ,p3)) s++;
            }
            syndiff[p1][p2][p3][X][Y][p3]
                =syndiff[X][Y][p3][p1][p2][p3]
                =double(s)/p;
	}

        // changes at 2nd and 3rd positions.
	if (aa(p1, Y, Z)!=TER) {
	    s=0;
    	    p=0;
	    // X11<->X21<->X22
            if (aa(p1,Y,p3)!=TER){
                p++;
                if (aa(p1,p2,p3)==aa(p1,Y ,p3)) s++;
                if (aa(p1,Y ,p3)==aa(p1,Y ,Z )) s++;
            }
	    // X11<->X12<->X22
            if (aa(p1,p2,Z)!=TER){
                p++;
                if (aa(p1,p2,p3)==aa(p1,p2,Z )) s++;
                if (aa(p1,p2,Z )==aa(p1,Y ,Z )) s++;
            }
            syndiff[p1][p2][p3][p1][Y][Z]
                =syndiff[p1][Y][Z][p1][p2][p3]
                =double(s)/p;
	}
        
        // changes at 1st and 3rd positions.
	if (aa(X, p2, Z)!=TER) {
	    s=0;
	    p=0;
	    // 1X1<->2X1<->2X2
            if (aa(X ,p2,p3)!=TER){
                p++;
                if (aa(p1,p2,p3)==aa(X ,p2,p3)) s++;
                if (aa(X ,p2,p3)==aa(X ,p2,Z )) s++;
            }
	    // 1X1<->1X2<->2X2
            if (aa(p1,p2,Z )!=TER){
                p++;
                if (aa(p1,p2,p3)==aa(p1,p2,Z )) s++;
                if (aa(p1,p2,Z )==aa(X ,p2,Z )) s++;
            }
            syndiff[p1][p2][p3][X][p2][Z]
                =syndiff[X][p2][Z][p1][p2][p3]
                =double(s)/p;
	}
    }
}

// 3 different sites.
void SynDiffClass::Diff3(){
    int s;
    int p;

    for (int p1=0;p1<4;p1++)
    for (int p2=0;p2<4;p2++)
    for (int p3=0;p3<4;p3++) if (aa(p1,p2,p3)==TER) continue; else
    for (int X=0;X<4;X++) if (X==p1) continue; else
    for (int Y=0;Y<4;Y++) if (Y==p2) continue; else
    for (int Z=0;Z<4;Z++) if (Z==p3) continue; else
    {
	s=0;
	p=0;
        // pathway1;  111<->211<->221<->222
        if (aa(X,p2,p3) != TER && aa(X,Y,p3) != TER) {
            p++;
            if (aa(p1,p2,p3)==aa(X ,p2,p3)) s++;
            if (aa(X ,p2,p3)==aa(X ,Y ,p3)) s++;
            if (aa(X ,Y ,p3)==aa(X ,Y ,Z )) s++;
        }

        // pathway2;  111<->211<->212<->222
        if (aa(X,p2,p3) != TER && aa(X,p2,Z) != TER) {
            p++;
            if (aa(p1,p2,p3)==aa(X ,p2,p3)) s++;
            if (aa(X ,p2,p3)==aa(X ,p2,Z )) s++;
            if (aa(X ,p2,Z )==aa(X ,Y ,Z )) s++;
        }

        // pathway3;  111<->121<->221<->222
        if (aa(p1,Y,p3) != TER && aa(X,Y,p3) != TER) {
            p++;
            if (aa(p1,p2,p3)==aa(p1,Y ,p3)) s++;
            if (aa(p1,Y ,p3)==aa(X ,Y ,p3)) s++;
            if (aa(X ,Y ,p3)==aa(X ,Y ,Z )) s++;
        }

        // pathway4;  111<->121<->122<->222
        if (aa(p1,Y,p3) != TER && aa(p1,Y,Z) != TER) {
            p++;
            if (aa(p1,p2,p3)==aa(p1,Y ,p3)) s++;
            if (aa(p1,Y ,p3)==aa(p1,Y ,Z )) s++;
            if (aa(p1,Y ,Z )==aa(X ,Y ,Z )) s++;
        }

        // pathway5;  111<->112<->212<->222
        if (aa(p1,p2,Z) != TER && aa(X,p2,Z) != TER) {
            p++;
            if (aa(p1,p2,p3)==aa(p1,p2,Z )) s++;
            if (aa(p1,p2,Z )==aa(X ,p2,Z )) s++;
            if (aa(X ,p2,Z )==aa(X ,Y ,Z )) s++;
        }

        // pathway6;  111<->112<->122<->222
        if (aa(p1,p2,Z) != TER && aa(p1,Y,Z) != TER) {
            p++;
            if (aa(p1,p2,p3)==aa(p1,p2,Z )) s++;
            if (aa(p1,p2,Z )==aa(p1,Y ,Z )) s++;
            if (aa(p1,Y ,Z )==aa(X ,Y ,Z )) s++;
        }
	if (p)
           syndiff[p1][p2][p3][X][Y][Z]
           =syndiff[X][Y][Z][p1][p2][p3]
           =double(s)/p;
    }
}

void SynDiffClass::Stop(){
    for (int p1=0;p1<4;p1++) 
    for (int p2=0;p2<4;p2++)
    for (int p3=0;p3<4;p3++)
	if (aa(p1,p2,p3)==TER)
	    for (int p4=0;p4<4;p4++) 
	    for (int p5=0;p5<4;p5++)
	    for (int p6=0;p6<4;p6++)
		syndiff[p1][p2][p3][p4][p5][p6]
		=syndiff[p4][p5][p6][p1][p2][p3]
		=-1;
}

void SynDiffClass::Print(int p1,int p2,int p3,int p4,int p5,int p6) const {
    double d  = DifferentSites(p1,p2,p3,p4,p5,p6);
    double ds = Diff(p1,p2,p3,p4,p5,p6);
    double dn = (aa(p1,p2,p3)!=TER && aa(p4,p5,p6)!=TER)?d-ds:-1;
    printf("%c%c%c(%c)-%c%c%c(%c) %.6f %.6f %.6f %c\n",
    	base(p1),base(p2),base(p3),aa(p1,p2,p3),
	base(p4),base(p5),base(p6),aa(p4,p5,p6),
	ds,dn,d,
	(p1==p4 && p2==p5 && p3==p6) ? '-' :
		aa(p1,p2,p3)==aa(p4,p5,p6) ? '=' : ' ');
}

void SynDiffClass::PrintAll() const {
    for (int p1=0;p1<4;p1++) 
    for (int p2=0;p2<4;p2++)
    for (int p3=0;p3<4;p3++)
	std::cout <<base(p1)<<base(p2)<<base(p3)<<aa(p1,p2,p3)<<": "
	     <<synsite[p1][p2][p3] nl;
    std::cout nl;

    for (int p1=0;p1<4;p1++) 
    for (int p2=0;p2<4;p2++)
    for (int p3=0;p3<4;p3++)
    for (int p4=0;p4<4;p4++) 
    for (int p5=0;p5<4;p5++)
    for (int p6=0;p6<4;p6++)
	Print(p1,p2,p3,p4,p5,p6);
}

void SynDiffClass::SynSiteTable(){
    for (int p1=0;p1<4;p1++)
    for (int p2=0;p2<4;p2++)
    for (int p3=0;p3<4;p3++){
	if (aa(p1,p2,p3)==TER) {
	    synsite[p1][p2][p3]=-1;
	    continue;
	}

        // 1st position
	int path=0;
	int syn=0;
	for (int X=0;X<4;X++)
	    if (X!=p1 && aa(X,p2,p3)!=TER) {
		path++;
		if (aa(X,p2,p3)==aa(p1,p2,p3))
		    syn++;
	    }
	synsite[p1][p2][p3]=double(syn)/path;

        // 2nd position
	path=0;
	syn=0;
	for (int X=0;X<4;X++)
	    if (X!=p2 && aa(p1,X,p3)!=TER){
		path++;
		if (aa(p1,X,p3)==aa(p1,p2,p3))
		    syn++;
	    }
	synsite[p1][p2][p3]+=double(syn)/path;

        // 3rd position
	path=0;
	syn=0;
	for (int X=0;X<4;X++)
	    if (X!=p3 && aa(p1,p2,X)!=TER){
		path++;
		if (aa(p1,p2,X)==aa(p1,p2,p3))
		    syn++;
	}
	synsite[p1][p2][p3]+=double(syn)/path;
    }
}
#endif //!defined(_SYNDIFFCLASS_H_)
