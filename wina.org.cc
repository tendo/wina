/*
	wina.cc - window analysis of ds and dn.
	Toshinori Endo <tendo@lab.nig.ac.jp>
	1996.8.23 first version
	1999.12.1 revised to adapt the new C++ specification

	$Id$
	$Log$
*/
//#define DEBUG


#define Infinite 999.9
#define NaN	 -1.0

//#include <stl.h>
#include <iomanip>
#include <math.h>
#include "CodonTab.h"
#include "common.h"
#include "synsite.h"
#include "SynDiffClass.h"

int WindowSize=60;
int Shift=3;


class omstream {
  std::ostream* ost;
  public:
    omstream(std::ostream& os=std::cout) { ost=&os; }
    template <class T&>
    omstream& operator<<(T& t) {
	*ost << t;
	return *this;
    }
    template <class T>
    omstream& operator<<(T t) {
	*ost << t;
	return *this;
    }
    omstream& operator<< (double f) {
	if (f == NaN)
	    *ost << "NaN";
	else if (f == Infinite)
	    *ost << "Inf";
	else if (f == 0)
	    *ost << 0.0;
	else
	    *ost << f;
	return *this;
    }
};

omstream out;

// Jukes-Cantor correction.
class Jukes_Cantor {
    double Diff;
    double Sites;
    double P;
    char* title;
  public:
    Jukes_Cantor(const double D,const double S, char* name="")
	 : title(name),Diff(D),Sites(S)
    {
	if (S != 0.0)
	    P = D/S;
	else
	    P = NaN;
    }
    ~Jukes_Cantor() {};
    const double diff() const { return Diff;}
    const double site() const { return Sites;}
    const double p() const { return P;}
    const double d() const {
	 return (P != NaN)
		? (P<.75)? -3.0/4.0 * log(1.0 - 4.0/3.0 * P): Infinite 
		: NaN;
    }
    const double v() const {
	 return (P != NaN)
		? (P<.75)? 9*P *(1-P)/((3-4*P)*(3-4*P)*Sites): Infinite
		: NaN;
    }
    const double se() const {
	 return (P != NaN) ? (P<.75)? sqrt(v()): Infinite : NaN;
    }
    const double Print() const {
	out << "  " << std::setw(16) << title << ' '
	    << std::setw(8) << std::setprecision(2) << diff() << " / "
	    << std::setw(8) << std::setprecision(2) << site() << ' '
	    << std::setw(8) << std::setprecision(4) << '('
	    << std::setw(8) << std::setprecision(4) << p() << ")\t"
	    << std::setw(8) << std::setprecision(4) << d()
	    << " +- " << se() nl;
    }
};

struct DistNGClass : public SeqArrayC,
	public SynSite, public SynDiffClass
{
    void DistNG(int,int,int,int) const; 
    void Print() const { SeqArrayC::Print();}
    void PrintDiffTbl(){ SynDiffClass::PrintAll();}
};

void DistNGClass::DistNG(int s1,int s2,int start=0,int end=0) const {
    int sites = 0;
    double syn_site = 0;
    double diff  = 0;
    double syn_D = 0;

    if (start<0)
	start=0;
    else if (start>MinLength())
	start=MinLength()-1;
    if (end==0 || end>MinLength())
	end=MinLength();
    else if (end<0 && end+MinLength()>start)
	end+=MinLength();

    for (int i=start; i<end; i+=3){
	if ((SharedSiteOnly==NO) || (IsShared(i)==YES)){
	    double s=Diff(Seq(s1,i),Seq(s2,i)); // synonymous difference
	    if (syn(Seq(s1,i))>=0 && syn(Seq(s2,i))>=0 && s>=0) {
		syn_site+=(syn(Seq(s1,i))+syn(Seq(s2,i)))/2;
		diff+=DifferentSites(Seq(s1,i),Seq(s2,i),3);
		syn_D+=s;
		sites+=3;
	    } /* else
		fprintf(stderr,"Bad Codon in eigther %s (%.3s) or %s (%.3s)\n",
			Name(s1),Seq(s1,i),Name(s2),Seq(s2,i));
	    */
	}
    }

    // Jukes-Cantor correction.
    Jukes_Cantor Syn(syn_D,syn_site,"Synonymous");
    Jukes_Cantor Nsy(diff-syn_D,sites-syn_site,"Nonsynonymous");

    // Output results.
    char mark[3]="  ";
    if (sites>=WindowSize-3){
	if (Syn.d()*2<Nsy.d()){
	    if (Nsy.d()<1.0)
		strcpy(mark,"**");
	    else
		strcpy(mark,"++");
	}
	else if (Syn.d()<Nsy.d()) {
	    if (Nsy.d()<1.0)
		strcpy(mark,"* ");
	    else
		strcpy(mark,"+ ");
	}
    }
    out << std::setw(5) << 1+start << "  "
	<< std::setw(8) << std::setprecision(4) << Syn.d() << "  " 
	<< std::setw(8) << std::setprecision(4) << Nsy.d() << ' '
	<< std::setw(2) << mark << ' '
	<< std::setw(5) << sites nl;
}

void run(std::istream& input){
    DistNGClass *List;
    while (!input.eof()){
	List=new DistNGClass;
	List->ReadAln(input);
#if defined(DEBUG)
	List->Print();
#else
	for (int i=0; i<List->Num()-1; i++)
	    for (int j=i+1; j<List->Num(); j++) {
		List->CheckSequence(i,j);
		out << std::setw(0) << '>'
		     << List->Name(i) << " x " << List->Name(j) nl;
		for (int x=0; x<=List->MinLength()-WindowSize; x+=Shift)
		    List->DistNG(i,j,x,x+WindowSize-1);
		out <<"//\n";
	    }
	delete List;
#endif
    }
}

void error(char* s,char* s2=""){
  std::cerr << s << ' ' << s2 << '\n';
  exit(1);
}


main(int argc, char* argv[])
{
    int n=0;
    for (int i=1; i<argc; i++){
        std::ifstream input(argv[i]);
        if (!input)
             error("cannot open input file",argv[1]);
 
        n++;
//	std::cout<< argv[i] nl;
        run(input);
    }
    if (n==0) run(std::cin);
}
