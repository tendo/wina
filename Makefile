CXX=g++ -g 
PATH:=.:$(PATH)

wina: wina.cc common.h SynDiffClass.h CodonTab.h synsite.h
	$(CXX) $(CPPFLAGS) $(CFLAGS) $< $(LDFLAGS) -o $@
test:
	wina test.aln

test2: out.ps
out.ps: update
	wina test.aln | wp >$@
	ps2pdf out.ps

lp:
	mp -A4 wina.cc common.h SynDiffClass.h CodonTab.h synsite.h makefile\
	| lp

update:
