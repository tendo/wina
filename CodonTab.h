/*
	CodonTab.cc - Codon Table and its related functions.
*/

#if !defined(__CODONTAB_H__)
#define __CODONTAB_H__

enum AminoAcids{STOP=0,PHE,TRP,TYR,HIS,MET,LEU,ILE,VAL,PRO,CYS,
                       ALA,GLY,THR,SER,GLN,ASN,LYS,ARG,GLU,ASP};

enum {TER='*',GAP,FRAMESHIFT,UNKNOWN};

const char* Codon_tab[] = {
	"AAA","AAC","AAG","AAT",  "ACA","ACC","ACG","ACT",
	"AGA","AGC","AGG","AGT",  "ATA","ATC","ATG","ATT",
	"CAA","CAC","CAG","CAT",  "CCA","CCC","CCG","CCT",
	"CGA","CGC","CGG","CGT",  "CTA","CTC","CTG","CTT",
	"GAA","GAC","GAG","GAT",  "GCA","GCC","GCG","GCT",
	"GGA","GGC","GGG","GGT",  "GTA","GTC","GTG","GTT",
	"TAA","TAC","TAG","TAT",  "TCA","TCC","TCG","TCT",
	"TGA","TGC","TGG","TGT",  "TTA","TTC","TTG","TTT",
	"---","<.>","???"
};

const char AA_tab[]={
	'K','N','K','N',  'T','T','T','T', 
	'R','S','R','S',  'I','I','M','I',
	'Q','H','Q','H',  'P','P','P','P',
	'R','R','R','R',  'L','L','L','L',
	'E','D','E','D',  'A','A','A','A',
	'G','G','G','G',  'V','V','V','V',
	'*','Y','*','Y',  'S','S','S','S',
	'*','C','W','C',  'L','F','L','F',
	'-','/','?'
};

const char* AA3_tab[]={
	"Lys","Asn","Lys","Asn",  "Thr","Thr","Thr","Thr",
	"Arg","Ser","Arg","Ser",  "Ile","Ile","MET","Ile",
	"Gln","His","Gln","His",  "Pro","Pro","Pro","Pro",
	"Arg","Arg","Arg","Arg",  "Leu","Leu","Leu","Leu",
	"Glu","Asp","Glu","Asp",  "Ala","Ala","Ala","Ala",
	"Gly","Gly","Gly","Gly",  "Val","Val","Val","Val",
	"Ter","Tyr","Ter","Tyr",  "Ser","Ser","Ser","Ser",
	"Ter","Cys","Trp","Cys",  "Leu","Phe","Leu","Phe",
	"---","<.>","???"
};

inline const char* Codon(int p1,int p2,int p3){
    if (0<=p1 && p1<4 && 0<=p2 && p2<4 && 0<=p3 && p3<4)
	return Codon_tab[p1*16+p2*4+p3];
    else
	return Codon_tab[UNKNOWN];
}

inline const char aa(int p1,int p2,int p3){
    if (0<=p1 && p1<4 && 0<=p2 && p2<4 && 0<=p3 && p3<4)
	return AA_tab[p1*16+p2*4+p3];
    else
	return AA_tab[UNKNOWN];
}
#endif

