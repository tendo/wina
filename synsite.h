#include "CodonTab.h"

class SynSite {
    double codon[4][4][4];
  public:
    SynSite();
    double syn(int p1,int p2,int p3) const {
	if (0<=p1 && p1<4 && 0<=p2 && p2<4 && 0<=p3 && p3<4)
	    return codon[p1][p2][p3];
	else
	    return -1;
    }
    double syn(const char* c) const {
	if (isbase(c[0]) && isbase(c[1]) && isbase(c[2]))
	    return codon[BaseNum(c[0])][BaseNum(c[1])][BaseNum(c[2])];
	return -1;
    }
    void print(){
	for (int p1=0; p1<4; p1++)
	for (int p2=0; p2<4; p2++)
	for (int p3=0; p3<4; p3++)
	    printf("%c%c%c: %.4f\n", base(p1),base(p2),base(p3),
			 codon[p1][p2][p3]);
    }
};

SynSite::SynSite(){
    for (int p1=0; p1<4; p1++)
    for (int p2=0; p2<4; p2++)
    for (int p3=0; p3<4; p3++)
	if (aa(p1,p2,p3)==TER)
	    codon[p1][p2][p3]=-1;
	else {
	    double p=0;  // possible change.
            double s=0;  // synonymous change.
	    for (int x=0; x<4; x++)
		if (x!=p1 && aa(x,p2,p3)!=TER){
		    p++;
		    if (aa(x,p2,p3)==aa(p1,p2,p3))
			s++;
		}
	    codon[p1][p2][p3]=s/p;
	    p=s=0;
	    for (int x=0; x<4; x++)
		if (x!=p2 && aa(p1,x,p3)!=TER){
		    p++;
		    if (aa(p1,x,p3)==aa(p1,p2,p3))
			s++;
		}
	    codon[p1][p2][p3]+=s/p;
	    p=s=0;
	    for (int x=0; x<4; x++)
		if (x!=p3 && aa(p1,p2,x)!=TER){
		    p++;
		    if (aa(p1,p2,x)==aa(p1,p2,p3))
			s++;
		}
	    codon[p1][p2][p3]+=s/p;
	}
}

#if 0
#include <iostream.h>
main(){
    SynSite s;

    for (int p1=0; p1<4; p1++)
    for (int p2=0; p2<4; p2++)
    for (int p3=0; p3<4; p3++)
	cout << Codon(p1,p2,p3) << " ("
	     << aa(p1,p2,p3) << ") "
	     << s.syn(p1,p2,p3,0) << " "
	     << s.syn(p1,p2,p3,1) << " "
	     << s.syn(p1,p2,p3,2) << endl;
}
#endif
