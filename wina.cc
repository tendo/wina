/*
	wina.cc - window analysis of ds and dn.
	Toshinori Endo <tendo@lab.nig.ac.jp>
	1996.8.23 first version
	1999.12.1 revised to adapt the new C++ specification

	$Id$
	$Log$
*/
//#define DEBUG


#define Infinite 999.9
#define NaN	 -1.0
#define Case   break; case
#define Default break; default


#include <iomanip>
#include <math.h>
#include "CodonTab.h"
#include "common.h"
#include "synsite.h"
#include "SynDiffClass.h"

int WindowSize=60;
int Shift=3;


class omstream {
  std::ostream* ost;
  public:
    omstream(std::ostream& os=std::cout) {
    	ost = &os;
    }
    /*
    template <typename T&>
    omstream& operator<<(T& t) {
	*ost<< t;
	return *this;
    }
    */
    template <class T>
    omstream& operator<<(T t) {
	*ost << t;
	return *this;
    }
    omstream& operator<< (double f) {
	if (f == NaN)
	    *ost << "NaN";
	else if (f == Infinite)
	    *ost << "Inf";
	else if (f == 0)
	    *ost << 0.0;
	else
	    *ost << f;
	return *this;
    }
};

omstream out;

// Jukes-Cantor correction.
class Jukes_Cantor {
    double Diff;
    double Sites;
    double P;
    const char* title;
  public:
    Jukes_Cantor(const double D,const double S, const char* name="")
	 : title(name),Diff(D),Sites(S)
    {
	if (S != 0.0)
	    P = D/S;
	else
	    P = NaN;
    }
    ~Jukes_Cantor() {};
    const double diff() const { return Diff;}
    const double site() const { return Sites;}
    const double p() const { return P;}
    const double d() const {
	 return (P != NaN)
		? (P<.75)? -3.0/4.0 * log(1.0 - 4.0/3.0 * P): Infinite 
		: NaN;
    }
    const double v() const {
	 return (P != NaN)
		? (P<.75)? 9*P *(1-P)/((3-4*P)*(3-4*P)*Sites): Infinite
		: NaN;
    }
    const double se() const {
	 return (P != NaN) ? (P<.75)? sqrt(v()): Infinite : NaN;
    }
    const double Print() const {
	/*
    	printf("  %16s %8.2f / %8.2f ( %8.4f )\t%8.4f +- %8.4f\n",
		 title, diff(), site(), p(), d(), se() );
	*/
	out << "  " << std::setw(16) << title << ' '
	    << std::setw(8) << std::setprecision(2) << diff() << " / "
	    << std::setw(8) << std::setprecision(2) << site() << "  ("
	    << std::setw(8) << std::setprecision(4) << p() << ")\t"
	    << std::setw(8) << std::setprecision(4) << d()
	    << " +- " << std::setw(8) << std::setprecision(4) << se() nl;
    }
};

struct DistNGClass : public SeqArrayC,
	public SynSite, public SynDiffClass
{
    void DistNG(int,int,int,int) const; 
    void Print() const { SeqArrayC::Print();}
    void PrintDiffTbl(){ SynDiffClass::PrintAll();}
};

void DistNGClass::DistNG(int s1,int s2,int start=0,int end=0) const {
    int sites = 0;
    double syn_site = 0;
    double diff  = 0;
    double syn_D = 0;

    if (start<0)
	start=0;
    else if (start>MinLength())
	start=MinLength()-1;
    if (end==0 || end>MinLength())
	end=MinLength();
    else if (end<0 && end+MinLength()>start)
	end+=MinLength();

    for (int i=start; i<end; i+=3){
	if ((SharedSiteOnly==NO) || (IsShared(i)==YES)){
	    double s=Diff(Seq(s1,i),Seq(s2,i)); // synonymous difference
	    if (syn(Seq(s1,i))>=0 && syn(Seq(s2,i))>=0 && s>=0) {
		syn_site+=(syn(Seq(s1,i))+syn(Seq(s2,i)))/2;
		diff+=DifferentSites(Seq(s1,i),Seq(s2,i),3);
		syn_D+=s;
		sites+=3;
	    } /* else
		fprintf(stderr,"Bad Codon in eigther %s (%.3s) or %s (%.3s)\n",
			Name(s1),Seq(s1,i),Name(s2),Seq(s2,i));
	    */
	}
    }

    // Jukes-Cantor correction.
    Jukes_Cantor Syn(syn_D,syn_site,"Synonymous");
    Jukes_Cantor Nsy(diff-syn_D,sites-syn_site,"Nonsynonymous");

    // Output results.
    char mark[3]="  ";
    if (sites>=WindowSize-3){
	if (Syn.d()*2<Nsy.d()){
	    if (Nsy.d()<1.0)
		strcpy(mark,"**");
	    else
		strcpy(mark,"++");
	}
	else if (Syn.d()<Nsy.d()) {
	    if (Nsy.d()<1.0)
		strcpy(mark,"* ");
	    else
		strcpy(mark,"+ ");
	}
    }
    /*
    printf("%5d  %7.4f  %8.4f %2s %5d\n",
    	start+1, Syn.d(), Nsy.d(), mark, sites);
	*/
    out << std::setw(5) << 1+start << "  "
	<< std::setw(8) << std::setprecision(4) << Syn.d() << "  " 
	<< std::setw(8) << std::setprecision(4) << Nsy.d() << ' '
	<< std::setw(2) << mark << ' '
	<< std::setw(5) << sites nl;
}

void run(std::istream& input){
    DistNGClass *List;
    while (!input.eof()){
	List=new DistNGClass;
	List->ReadAln(input);
#if defined(DEBUG)
	List->Print();
#else
	for (int i=0; i<List->Num()-1; i++)
	    for (int j=i+1; j<List->Num(); j++) {
		List->CheckSequence(i,j);
		out << std::setw(0) << '>'
		     << List->Name(i) << " x " << List->Name(j) nl;
		for (int x=0; x<=List->MinLength()-WindowSize; x+=Shift)
		    List->DistNG(i,j,x,x+WindowSize-1);
		out <<"//\n";
	    }
	delete List;
#endif
    }
}

void error(char* s,char* s2=""){
  std::cerr << s << ' ' << s2 << '\n';
  exit(1);
}


main(int argc, char* const argv[])
{
 
    extern char *optarg;
    extern int optind, opterr, optopt;
    int c;

    while ((c = getopt(argc, argv, "w:s:n:")) != -1)
       switch (c) {
         case 'w': WindowSize = atoi(optarg);
         Case 's': Shift = atoi(optarg);
//       Case 'l': LINELEN = atoi(optarg);
         Case 'n': NAMELEN = atoi(optarg);
         Default:  std::cerr << "Wina: window analysis for dn, ds ver 0.34" nl
                        << "(C) Toshinori Endo <endo@nav.to> 1995-2004" nl
                        << "Resdistribution is granted under the condition of GPL." nl
                        << "See http://www.opensource.org/licenses/gpl-license.php for GPL." nl
                        << "Usage: " nl
//                      << "wina [-wsnl]" nl nl
                        << "wina [-wsn]"  nl nl
                        << "Options (default)" nl
                        << "  -w : WindowSize (60)" nl
                        << "  -s : ShiftSize   (3)" nl
//                      << "  -l : Line width (60)" nl
                        << "  -n : Name width (10)" nl nl;
                   exit(1);
       }

//    std::cout << WindowSize << ", " << Shift << ", " <<NAMELEN nl;
    if (optind < argc)
       while (optind < argc){
            std::ifstream input(argv[optind++]);
            if (!input)
               error((char*)"cannot open input file",argv[1]);

            run(input);
        }
    else
       run(std::cin);
}
